# sourcedata
Simple dynamic JS datasource object, useful to combine with observables like Vue

***

##Installation

Install **sourcedata** with Bower or NPM.

###Bower

`bower i -S sourcedata`

###NPM

`npm i -S sourcedata`

With JSPM you can install **sourcedata** from NPM:

###JSPM

`jspm i sourcedata=npm:sourcedata`

##Usage

It's possible to use **sourcedata** with module loaders or globally.

###ES6

```javascript
import SourceData from 'sourcedata'
```

###CommonJS

```javascript
let SourceData = require('sourcedata')
```

###AMD

```javascript
require(['sourcedata'], (SourceData) => {
	// code goes here
})
```

###Global

```html
<script src="path/to/sourcedata.min.js"></script>
```

##Instance

**sourcedata** contains a class object that can be instantiated to a variable.

```javascript
let sdata = new SourceData(opt)
```

An `opt` object can setup SourceData as follows (default values):

```javascript
let sdata = new SourceData({
	source: [],	// Array with initial data.
	size: 0	// Page size, items per page. 0 = no pagination
})

// Or simply: let sdata = new SourceData
```

##Properties

- **total**: total data items count
- **size**: the page size, items per page
- **pages**: pages count
- **page**: current page number
- **data**: result data items (filtered and/or sorted, if any)
- **view**: current page items
- **prev**: previews page number (or 0 if none)
- **next**: next page number (or 0 if none)

##Methods

- **pgPrev()**: navigates to previews page
- **pgNext()**: navigates to next page
- **pgFirst()**: navigates to first page
- **pgLast()**: navigates to last page
- **filter(fn)**: native JS Array.filter method to filter data (opt in)
- **sort(fn)**: native JS Array.sort method to sort data (opt in)
- **getItem(fn)**: gets an item with native JS Array.find method, returns { index: i, value: item }
- **setItem(obj)**: replaces an item value, obj argument needs to be like { index: i, value: item }
- **addItem(item)**: adds new item to data source using native JS Array.push
- **delItem(index)**: deletes an item from data source using native JS Array.splice
- **items(method, arg)**: executes a `method` with `arg`, `method` can be `filter`, `sort`, `get`, `set`, `add`, `del`,

##TODO

- ~~Filter/Sort items~~
- ~~Get/Set items~~
- ~~Add/Del items~~
- Observable events
