'use strict';
new Vue({
	el: 'body',
	data: {
		list: new SourceData({
			source: [4, 5, 6, 1, 2, 3, 7, 8, 9],
			size: 1,
			filter: function(i) {
				return i % 2
			},
			sort: function(a, b) {
				return b - a
			}
		}),
		szs: [1, 2, 3, 4, 5],
		sz: 1
	},
	watch: {
		sz(val) {
			this.list.size = parseInt(val)
		}
	},
	methods: {
		edit() {
			let list = this.$data.list,
				item = list.getItem(i => i > 8)

			item.value += 2
			list.setItem(item)
		}
	}
})
